enum Special {
    INodeNum(u32),   /// indirect node number (hard links only)
    LinkCount(u32),  /// links that refer to this indirect node
    RawDevice(u32),  /// special file device (FBLK and FCHR only)
}

struct HFSPlusBSDInfo {
    ownerID:    u32,     /// user-id of owner or hard link chain previous link
    groupID:    u32,     /// group-id of owner or hard link chain next link
    adminFlags: u8,      /// super-user changeable flags
    ownerFlags: u8,      /// owner changeable flags
    fileMode:   u16,     /// file type and permission bits
    special:    Special      
}